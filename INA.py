#!/usr/bin/env python3

import os
import sys
import glob
import signal
import subprocess
import traceback
import urllib.request
from threading import Thread
from socket import timeout

rootdir: str = os.path.abspath(os.path.dirname(__file__))
pkgdatadir: str = "src"

signal.signal(signal.SIGINT, signal.SIG_DFL)

if __name__ == "__main__":
    import gi

    gi.require_version("Gdk", "3.0")
    gi.require_version("Gtk", "3.0")
    gi.require_version("Notify", "0.7")

    from gi.repository import Gio, Gtk

    # Update
    if "--no-version-check" not in sys.argv and "-v" not in sys.argv:

        def update_if_needed():
            CURRENT_VERSION_URL = "https://gitlab.com/czarlie/ina/-/raw/master/VERSION"
            LOCAL_VERSION_PATH = os.path.join(rootdir, "VERSION")
            GIT_REPO_URL = "https://gitlab.com/czarlie/ina"

            try:
                with open(LOCAL_VERSION_PATH, "r") as version_file:
                    request = urllib.request.Request(
                        CURRENT_VERSION_URL, headers={"User-Agent": "INA"}
                    )
                    with urllib.request.urlopen(request, timeout=10) as online_version:
                        if float(version_file.read()) < float(online_version.read().decode()):
                            subprocess.call(
                                ("git", "-C", os.path.dirname(__file__), "pull", GIT_REPO_URL,)
                            )

            except (urllib.error.URLError, urllib.error.HTTPError, timeout):
                print("Could not check version")
                traceback.print_exc()

        Thread(target=update_if_needed).start()

    os.chdir(os.path.join(rootdir, pkgdatadir))

    for resource_xml in glob.glob("**/*.gresource.xml", recursive=True):
        os.chdir(os.path.dirname(resource_xml) or ".")

        subprocess.run(("glib-compile-resources", os.path.basename(resource_xml)))
        resource: Gio.Resource = Gio.Resource.load(
            os.path.basename(resource_xml)[::-1].replace(".xml"[::-1], "", 1)[::-1]
        )
        resource._register()

        os.chdir(os.path.join(rootdir, pkgdatadir))

    os.chdir(rootdir)

    Gtk.IconTheme.get_default().add_resource_path("/org/czarlie/Ina/icons")

    print(Gtk.IconTheme.get_default().has_icon("ina-notes"))

    if sys.platform.startswith("linux"):
        with open("INA.desktop", "r") as desktop_file:
            desktop_text = desktop_file.read().format(
                exec=os.path.abspath(__file__),
                icon=os.path.join(rootdir, "src", "icons", "ina-notes.svg"),
            )

        with open(
            os.path.expanduser("~/.local/share/applications/ina-notes.desktop"), "w",
        ) as desktop_file:
            desktop_file.write(desktop_text)

    from src import main

    sys.exit(main.main())
