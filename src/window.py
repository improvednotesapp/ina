# window.py
#
# Copyright 2020 Czarlie
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE X CONSORTIUM BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
# Except as contained in this notice, the name(s) of the above copyright
# holders shall not be used in advertising or otherwise to promote the sale,
# use or other dealings in this Software without prior written
# authorization.

from typing import Dict, List, Type, Optional

from os import path, mkdir, remove, listdir, rename
from sys import platform
from time import time

from .segments.__segment__ import Segment
from .segment_container import (
    SegmentContainer,
    DND_ENTRIES,
    HANDLES,
    INA_SEGMENT_ATOM,
)
from .persistentdict import PersistentDict
from .random_base64 import random_string
from . import colors

from gi.repository import Gio, GLib, Gdk, Gtk, Notify

STYLE = """
@define-color win-bg rgb{bg};
@define-color win-contrast rgb{contrast};
"""


def iswindows() -> bool:
    return platform.startswith("win")


@Gtk.Template(resource_path="/org/czarlie/Ina/window.ui")
class InaWindow(Gtk.ApplicationWindow):
    __gtype_name__ = "InaWindow"

    title_entry: Gtk.Entry = Gtk.Template.Child()

    toolbox_stack: Gtk.Stack = Gtk.Template.Child()
    toolbox_revealer: Gtk.Revealer = Gtk.Template.Child()

    content_revealer: Gtk.Revealer = Gtk.Template.Child()

    control_button_revealer: Gtk.Revealer = Gtk.Template.Child()

    more_button: Gtk.Button = Gtk.Template.Child()
    expand_button: Gtk.Button = Gtk.Template.Child()
    collapse_button: Gtk.Button = Gtk.Template.Child()

    color_chooser: Gtk.ColorChooser = Gtk.Template.Child()
    color_chooser_button: Gtk.Button = Gtk.Template.Child()
    color_chooser_popover: Gtk.Popover = Gtk.Template.Child()

    collapse_button_stack: Gtk.Stack = Gtk.Template.Child()

    segment_list_box: Gtk.ListBox = Gtk.Template.Child()
    segment_drop_target: Gtk.EventBox = Gtk.Template.Child()

    segment_add_button_box: Gtk.Box = Gtk.Template.Child()
    more_segment_types_button: Gtk.Button = Gtk.Template.Child()
    segment_add_more_button_box: Gtk.Box = Gtk.Template.Child()
    more_segments_popover: Gtk.Popover = Gtk.Template.Child()

    scrolled_window: Gtk.ScrolledWindow = Gtk.Template.Child()
    viewport: Gtk.Viewport = Gtk.Template.Child()

    def __init__(
        self, segment_types: Dict[str, Type[Segment]], note_id: Optional[str] = None, **kwargs
    ):
        """Create a new InaWindow.

        Args:
            segment_types (Dict[str, Type[Segment]]): A list of classes that subclass Segment
                which represent segment types that can be added to this note
            note_id (Optional[str]): The ID of this note or None to generate a new ID, default: None
            **kwargs: Arguments for Gtk.ApplicationWindow
        """
        super().__init__(**kwargs)
        self.set_icon_name("ina-notes")

        self.add_actions()

        self.init_pdict(note_id)
        self.init_add_buttons(segment_types)
        self.init_color_picker()

        self.css_provider = Gtk.CssProvider()
        self.add_style(self)
        self.add_style(self.more_segments_popover)
        self.add_style(self.color_chooser_popover)

        self.title_entry.grab_focus()  # This 'fixes' the placeholder color

        self.displayed_segments: Dict[str, SegmentContainer] = {}
        self.init_dnd()
        self.init_title()
        self.init_menu()

        self.add_listeners()
        self.connect_signals()

        self.last_zoomed = time()

        GLib.idle_add(self.scroll)

    def init_pdict(self, note_id: Optional[str]) -> None:
        """Initialise the PersistentDict used to represent this note.

        Args:
            note_id (Optional[str]): The ID of this note or None to generate a new ID
        """
        self.note_id = note_id
        if note_id is None:
            self.note_id = random_string(64)

        try:
            mkdir("notes")
        except FileExistsError:
            pass

        self.pdict = PersistentDict(path.join("notes", self.note_id + ".json"))

        self.app_pdict = self.get_application().pdict

    def init_add_buttons(self, segment_types: Dict[str, Type[Segment]]) -> None:
        """Initialise the add buttons at the bottom of the window."""
        self.segment_types: Dict[str, Type[Segment]] = segment_types

        for segment_type_id, segment_type in list(self.segment_types.items())[:3]:
            segment_image = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
            segment_image.pack_start(
                Gtk.Image.new_from_icon_name("list-add-symbolic", Gtk.IconSize.LARGE_TOOLBAR),
                expand=False,
                fill=False,
                padding=0,
            )
            segment_image.pack_start(
                Gtk.Image.new_from_gicon(segment_type.icon, Gtk.IconSize.LARGE_TOOLBAR),
                expand=False,
                fill=False,
                padding=0,
            )
            segment_image.set_border_width(4)
            segment_image.show_all()

            segment_add_button = Gtk.Button()
            segment_add_button.set_tooltip_text("Add {}".format(segment_type.name))
            segment_add_button.set_image(segment_image)
            segment_add_button.set_always_show_image(True)
            segment_add_button.set_can_focus(False)
            self.segment_add_button_box.pack_start(
                segment_add_button, expand=False, fill=False, padding=0
            )

            segment_add_button.connect("clicked", self.on_add_segment_clicked, segment_type_id)

        for segment_type_id, segment_type in list(self.segment_types.items())[3:]:
            segment_image = Gtk.Image.new_from_gicon(segment_type.icon, Gtk.IconSize.LARGE_TOOLBAR)
            segment_image.set_margin_top(4)
            segment_image.set_margin_bottom(4)
            segment_image.show()

            segment_add_button = Gtk.Button.new_with_label(segment_type.name)
            segment_add_button.set_tooltip_text("Add {}".format(segment_type.name))
            segment_add_button.set_image(segment_image)
            segment_add_button.set_always_show_image(True)
            segment_add_button.set_can_focus(False)
            self.segment_add_more_button_box.pack_start(
                segment_add_button, expand=False, fill=False, padding=0
            )

            segment_add_button.connect("clicked", self.on_add_segment_clicked, segment_type_id)

            self.more_segment_types_button.show()

        self.segment_add_button_box.show_all()
        self.segment_add_more_button_box.show_all()

    def init_title(self):
        self.title_entry.set_text(self.pdict.get("title", ""))

    def init_menu(self):
        menu_builder = Gtk.Builder.new_from_resource("/org/czarlie/Ina/window_menu.xml")
        model = menu_builder.get_object("window_menu")
        model.freeze()

        self.get_application().set_app_menu(model)

        self.menu = Gtk.Menu.new_from_model(model)
        self.menu.attach_to_widget(self.more_button)

    def init_color_picker(self):
        color_choices = []
        hex_colors = self.get_application().pdict.get(
            "default_color_choices",
            [
                *["#d2e5b7", "#e5e2b7", "#e5cfb7", "#e5b7b7", "#e5c3e0"],
                *["#a4a0e5", "#85bae5", "#ace5d4", "#333333", "#eeeeee"],
                *["#94b26b", "#ccc67a", "#b2916b", "#b26b6b", "#b26ba8"],
                *["#6d6bb2", "#6b92b2", "#6bb29d", "#555555", "#cccccc"],
                *["#2D3324", "#333224", "#332B24", "#332424", "#332431"],
                *["#2D2C4C", "#2E3F4C", "#2D403A", "#777777", "#aaaaaa"],
            ],
        )

        for hex_color in hex_colors:
            rgba = Gdk.RGBA()
            rgba.parse(hex_color)
            color_choices.append(rgba)

        self.color_chooser.add_palette(Gtk.Orientation.HORIZONTAL, 10, color_choices)
        self.color_chooser_visible: bool = False

    def init_dnd(self):
        self.segment_drop_target.drag_dest_set(
            Gtk.DestDefaults.ALL, DND_ENTRIES, Gdk.DragAction.MOVE,
        )
        self.segment_drop_target.connect("drag-data-received", self.on_drag_data_received)
        self.segment_drop_target.connect("drag-motion", self.on_drag_motion)

    def add_listeners(self):
        self.on_update_collapsed(self.pdict.get("collapsed", False))
        self.pdict.add_change_listener("collapsed", self.on_update_collapsed)

        self.on_update_background(
            self.pdict.get("background", self.app_pdict.get("default_note_background", "#333333"),)
        )
        self.pdict.add_change_listener("background", self.on_update_background)

        self.configuring = False
        self.on_update_window_size(self.pdict.get("window_size", list(self.get_size())))
        self.pdict.add_change_listener("window_size", self.on_update_window_size)

        self.on_update_window_position(self.pdict.get("window_position", list(self.get_position())))
        self.pdict.add_change_listener("window_position", self.on_update_window_position)

        self.on_update_window_maximized(self.pdict.get("window_maximized", False))
        self.pdict.add_change_listener("window_maximized", self.on_update_window_maximized)

        self.updating_segment_list = False
        self.on_update_segments(self.pdict.get("segments", []))
        self.pdict.add_change_listener("segments", self.on_update_segments)

        self.on_update_scale(self.pdict.get("scale", 1.0))
        self.pdict.add_change_listener("scale", self.on_update_scale)

        self.on_update_archived(self.pdict.get("archived", False))
        self.pdict.add_change_listener("archived", self.on_update_archived)

    def connect_signals(self):
        self.connect("set-focus", self.on_set_focus)
        self.connect("configure-event", self.on_configure_event)

        self.title_entry.connect("changed", self.on_title_entry_changed)

        self.scrolled_window.get_vadjustment().connect("value-changed", self.on_vscroll)
        self.scrolled_window.get_hadjustment().connect("value-changed", self.on_hscroll)

        self.expand_button.connect("clicked", self.on_expand_clicked)
        self.collapse_button.connect("clicked", self.on_collapse_clicked)

        self.more_button.connect("clicked", self.on_more_clicked)

        self.color_chooser_button.connect("clicked", self.on_color_chooser_button_clicked)
        self.color_chooser_popover.connect("hide", self.on_color_chooser_popover_hide)
        self.color_chooser.connect("notify::rgba", self.on_color_pick)

        self.color_chooser.connect("color-activated", self.on_color_activated)

    def add_style(self, widget: Gtk.Widget):
        widget.get_style_context().add_provider(
            self.css_provider, Gtk.STYLE_PROVIDER_PRIORITY_SETTINGS
        )

        if isinstance(widget, Gtk.Container):
            for child in widget.get_children():
                self.add_style(child)

            if isinstance(widget, Gtk.ScrolledWindow):
                self.add_style(widget.get_vscrollbar())
                self.add_style(widget.get_hscrollbar())

    def add_actions(self):
        delete_action = Gio.SimpleAction.new("delete", None)
        delete_action.connect("activate", self.on_delete)
        self.add_action(delete_action)

        zoom_in_action = Gio.SimpleAction.new("zoom-in", None)
        zoom_in_action.connect("activate", self.on_zoom_in)
        self.add_action(zoom_in_action)

        zoom_out_action = Gio.SimpleAction.new("zoom-out", None)
        zoom_out_action.connect("activate", self.on_zoom_out)
        self.add_action(zoom_out_action)

        zoom_reset_action = Gio.SimpleAction.new("zoom-reset", None)
        zoom_reset_action.connect("activate", self.on_zoom_reset)
        self.add_action(zoom_reset_action)

    def scroll(self):
        self.scrolled_window.get_vadjustment().set_value(self.pdict.get("vscroll", 0))

        self.scrolled_window.get_hadjustment().set_value(self.pdict.get("hscroll", 0))

    def add_segment(self, segment_type_id: str) -> None:
        try:
            mkdir("segments")
        except FileExistsError:
            pass

        segment_id = random_string(64)
        segment_pdict = PersistentDict(path.join("segments", segment_id + ".json"))

        segment_pdict.set("id", segment_id)
        segment_pdict.set("type_id", segment_type_id)

        self.pdict.set(
            "segments", self.pdict.get("segments", []) + [[segment_type_id, segment_id]],
        )

        self.displayed_segments[segment_id].focus_content()

    def add_toolbox(self, toolbox: Optional[Gtk.Widget], segment_id: str):
        if toolbox is not None:
            if self.toolbox_stack.get_child_by_name(segment_id) is None:
                box = Gtk.Box(Gtk.Orientation.VERTICAL)

                if isinstance(toolbox, Gtk.Orientable):
                    toolbox.set_orientation(Gtk.Orientation.VERTICAL)

                box.pack_start(toolbox, expand=False, fill=False, padding=0)
                self.add_style(box)
                box.show_all()

                self.toolbox_stack.add_named(box, segment_id)

    def create_segment_container(self, segment_type_id: str, segment_id: str):
        segment_pdict: PersistentDict = PersistentDict(path.join("segments", segment_id + ".json"))
        segment_pdict.set("id", segment_id)
        segment_pdict.set("type_id", segment_type_id)

        segment_container: SegmentContainer = SegmentContainer(
            self.segment_types[segment_type_id](segment_pdict), self
        )

        if segment_id == self.pdict.get("last_focus", ""):
            GLib.idle_add(segment_container.focus_content)

        self.displayed_segments[segment_id] = segment_container

        self.add_style(segment_container)

    def update_segment_list(self):
        self.updating_segment_list = True
        segments = []

        self.displayed_segments = {}

        for row in self.segment_list_box:
            seg_pdict = row.get_child().segment.get_pdict()
            segments.append((seg_pdict.get("type_id"), seg_pdict.get("id")))

            self.displayed_segments[seg_pdict.get("id")] = row.get_child()

        segment_set = set(segments)
        segments = sorted(segment_set, key=segments.index)

        self.pdict.set("segments", [list(segment) for segment in segments])
        self.updating_segment_list = False

    def soft_delete(self):
        self.pdict.set("deleted", True)

        if "../" not in self.note_id:  # is not UNA system note
            if iswindows() or self.get_application().pdict.get("delete_undo_timeout", 3) == 0:
                warn_dialog = Gtk.MessageDialog(
                    text=(
                        "Delete "
                        + (
                            "untitled note"
                            if self.pdict.get("title", "").strip() == ""
                            else self.pdict.get("title", "").strip()
                        )
                        + "?"
                    ),
                    secondary_text=("This action cannot be undone."),
                    buttons=Gtk.ButtonsType.YES_NO,
                    message_type=Gtk.MessageType.QUESTION,
                    transient_for=self,
                )

                response = warn_dialog.run()

                if response == Gtk.ResponseType.YES:
                    self.force_delete()
                    self.hide()

                warn_dialog.close()

            else:
                if self.get_application().last_notification is not None:
                    self.get_application().last_notification.close()

                if "wastebasket" not in listdir():
                    mkdir("wastebasket")

                rename(
                    path.join("notes", self.note_id + ".json"),
                    path.join("wastebasket", self.note_id + ".json"),
                )
                self.pdict.file = path.join("wastebasket", self.note_id + ".json")

                if self.pdict.get("title", "").strip() == "":
                    self.restore_notif = Notify.Notification.new(
                        "Note deleted", "Deleted untitled note", "ina-notes",
                    )
                else:
                    self.restore_notif = Notify.Notification.new(
                        "Note deleted", 'Deleted "' + self.pdict.get("title") + '"', "ina-notes",
                    )

                self.get_application().last_notification = self.restore_notif

                self.restore_notif.add_action("restore", "Undo", self.restore_deleted)
                self.restore_notif.add_action(
                    "delete_permanently", "Dismiss", self.clean_up_if_deleted
                )

                self.restore_notif.connect("closed", self.clean_up_if_deleted)

                self.restore_notif.set_urgency(Notify.Urgency.CRITICAL)

                GLib.timeout_add_seconds(
                    priority=0,
                    interval=self.get_application().pdict.get("delete_undo_timeout", 3),
                    function=self.clean_up_if_deleted,
                )

                self.restore_notif.show()

                self.hide()
        else:
            self.force_delete()

    def restore_deleted(self, *args):
        self.get_application().withdraw_notification("deleted")
        self.pdict.set("deleted", False)
        rename(
            path.join("wastebasket", self.note_id + ".json"),
            path.join("notes", self.note_id + ".json"),
        )
        self.pdict.file = path.join("notes", self.note_id + ".json")
        self.show()

    def clean_up_if_deleted(self, *args):
        if self.get_application() is not None:
            self.get_application().last_notification = None

        try:
            for waste_note in listdir("wastebasket"):
                remove(path.join("wastebasket", waste_note))
        except FileNotFoundError:
            return

        self.close()

        for segment_type_id, segment_id in self.pdict.get("segments"):
            try:
                remove(path.join("segments", segment_id + ".json"))
            except FileNotFoundError:
                pass

    def force_delete(self):
        self.close()

        for segment_type_id, segment_id in self.pdict.get("segments"):
            try:
                remove(path.join("segments", segment_id + ".json"))
            except FileNotFoundError:
                pass

        try:
            remove(self.pdict.file)
        except FileNotFoundError:
            pass

    def on_add_segment_clicked(self, button: Gtk.Button, segment_type: Type[Segment]) -> None:
        self.add_segment(segment_type)
        self.more_segments_popover.hide()

    def on_title_entry_changed(self, *args) -> None:
        title = self.title_entry.get_text()

        if title.strip() == "":
            title = "Untitled note"

        self.pdict.set("title", title)

    def on_set_focus(self, window: Gtk.Window, focus_widget: Gtk.Widget):
        if focus_widget is None:
            return

        segment_container = focus_widget.get_ancestor(SegmentContainer)
        next_stack = focus_widget.get_ancestor(Gtk.Stack)
        popover = focus_widget.get_ancestor(Gtk.Popover)

        if popover is not None:
            reveal = self.toolbox_revealer.get_reveal_child()

        elif segment_container is not None:
            self.pdict.set(
                "last_focus",
                path.basename(segment_container.segment.get_pdict().file).split(".", 1)[0],
            )

            if (
                self.toolbox_stack.get_child_by_name(
                    segment_container.segment.get_pdict().get("id")
                )
                is not None
            ):
                reveal = True
                self.toolbox_stack.set_visible_child_name(
                    segment_container.segment.get_pdict().get("id")
                )
            else:
                reveal = False

        elif next_stack is self.toolbox_stack:
            reveal = True

        else:
            reveal = False

        if reveal:
            self.toolbox_revealer.set_reveal_child(True)

        else:
            self.toolbox_revealer.set_reveal_child(False)

        for row in self.segment_list_box.get_children():
            row.get_child().on_window_set_focus(window, focus_widget)

    def on_vscroll(self, *args):
        self.pdict.set("vscroll", self.scrolled_window.get_vadjustment().get_value())

    def on_hscroll(self, *args):
        self.pdict.set("hscroll", self.scrolled_window.get_hadjustment().get_value())

    def on_drag_data_received(
        self,
        widget: Gtk.Widget,
        context: Gdk.DragContext,
        x: int,
        y: int,
        data: Gtk.SelectionData,
        info: int,
        time: int,
    ):
        handle: Gtk.Widget = HANDLES[data.get_data().decode()]
        source: Gtk.ListBoxRow = handle.get_ancestor(Gtk.ListBoxRow)
        target: Gtk.ListBoxRow = self.segment_list_box.get_row_at_y(y)

        if source is target:
            return

        source_list: Gtk.ListBox = source.get_ancestor(Gtk.ListBox)
        target_list: Gtk.ListBox = self.segment_list_box

        source_list.remove(source)

        if target is None:
            position = -1
        else:
            target_index = target.get_index()

            alloc = target.get_allocation()

            wx, wy = target.get_pointer()
            if wy > alloc.height / 2:
                position = target_index + 1
            else:
                position = target_index

        target_list.insert(source, position)

        self.update_segment_list()
        source_list.get_toplevel().update_segment_list()

        if isinstance(source.get_child(), SegmentContainer):
            source.get_child().set_scale(self.pdict.get("scale", 1.0))
            self.add_toolbox(
                source.get_child().segment.get_toolbox(),
                source.get_child().segment.get_pdict().get("id"),
            )

        if (
            source_list.get_toplevel().pdict.get("title") in {"Untitled note", ""}
            and len(source_list.get_toplevel().pdict.get("segments")) == 0
        ):
            source_list.get_toplevel().force_delete()

    def on_drag_motion(
        self, widget: Gtk.Widget, context: Gdk.DragContext, x: int, y: int, time: int,
    ):
        for handle in HANDLES.values():
            ancestor = handle.get_ancestor(SegmentContainer)

            if ancestor is not None:
                ancestor.unset_state_flags(Gtk.StateFlags.DROP_ACTIVE)
                ancestor.get_child().get_style_context().remove_class("drop-above")
                ancestor.get_child().get_style_context().remove_class("drop-below")

        if INA_SEGMENT_ATOM in context.list_targets():
            target: Gtk.ListBoxRow = self.segment_list_box.get_row_at_y(y)

            if target is None and len(self.segment_list_box.get_children()):
                target = self.segment_list_box.get_children()[-1]

            if target is not None:
                target.get_child().set_state_flags(Gtk.StateFlags.DROP_ACTIVE, clear=False)

                alloc = target.get_allocation()
                wx, wy = target.get_pointer()

                if wy > alloc.height / 2:
                    target.get_child().get_style_context().add_class("drop-below")
                    target.get_child().get_style_context().remove_class("drop-above")
                else:
                    target.get_child().get_style_context().add_class("drop-above")
                    target.get_child().get_style_context().remove_class("drop-below")

            else:
                widget.set_state_flags(Gtk.StateFlags.DROP_ACTIVE, clear=False)

                widget.get_style_context().add_class("no-segments")

            return True
        return False

    def on_more_clicked(self, *args):
        self.menu.popup_at_widget(
            self.more_button, Gdk.Gravity.SOUTH_EAST, Gdk.Gravity.NORTH_EAST, None,
        )

    def on_collapse_clicked(self, *args):
        self.pdict.set("collapsed", True)

    def on_expand_clicked(self, *args):
        self.pdict.set("collapsed", False)

    def on_color_chooser_button_clicked(self, *args):
        self.color_chooser_visible = True
        self.color_chooser.set_property("show-editor", False)

    def on_color_chooser_popover_hide(self, *args):
        self.color_chooser_visible = False

    def on_color_pick(self, *args):
        rgba: Gdk.RGBA = self.color_chooser.get_rgba()
        self.pdict.set(
            "background",
            colors.rgb_to_hex(colors.rgb1a_to_rgb255((rgba.red, rgba.green, rgba.blue))),
        )

    def on_color_activated(self, *args):
        self.color_chooser_popover.hide()

    def on_configure_event(self, window: "InaWindow", event: Gdk.EventConfigure):
        self.configuring = True

        self.pdict.set("window_maximized", self.is_maximized())

        if not self.is_maximized():
            self.pdict.set("window_size", [event.width, event.height])
            self.pdict.set("window_position", self.get_position())

        self.configuring = False

    def on_delete(self, *args):
        self.soft_delete()

    def on_zoom_in(self, *args):
        if (time() - self.last_zoomed) * 1000 > self.get_application().pdict.get(
            "zoom_interval", 50
        ):
            self.pdict.set("scale", min(self.pdict.get("scale", 1.0) * 1.125, 1.125 ** 5))
            self.last_zoomed = time()

    def on_zoom_out(self, *args):
        if (time() - self.last_zoomed) * 1000 > self.get_application().pdict.get(
            "zoom_interval", 50
        ):
            self.pdict.set("scale", max(self.pdict.get("scale", 1.0) / 1.125, 1.125 ** -10))
            self.last_zoomed = time()

    def on_zoom_reset(self, *args):
        self.pdict.set("scale", 1)

    # Handlers for pdict

    def on_update_background(self, background):
        bg = colors.string_to_rgb255(background)
        contrast = colors.contrast(
            bg,
            *[
                colors.string_to_rgb255(color)
                for color in self.app_pdict.get("foreground_color_options", ["#222222", "#ffffff"])
            ],
        )
        ccontrast = colors.contrast(
            contrast,
            *[
                colors.string_to_rgb255(color)
                for color in self.app_pdict.get("foreground_color_options", ["#222222", "#ffffff"])
            ],
        )
        self.css_provider.load_from_data(
            STYLE.format(bg=bg, contrast=contrast, ccontrast=ccontrast).encode("utf-8")
        )

        rgba: Gdk.RGBA = Gdk.RGBA()
        rgba.parse(background)
        self.color_chooser.set_rgba(rgba)

    def on_update_collapsed(self, collapsed: bool) -> None:
        if collapsed:
            self.collapsed = True

            self.content_revealer.set_reveal_child(False)
            self.control_button_revealer.set_reveal_child(False)
            self.set_resizable(False)

            self.collapse_button_stack.set_visible_child_name("expand")

            self.get_style_context().add_class("collapsed")

        else:
            self.content_revealer.set_reveal_child(True)
            self.control_button_revealer.set_reveal_child(True)

            def restore():
                self.set_resizable(True)

            GLib.timeout_add(200, restore)

            self.collapse_button_stack.set_visible_child_name("collapse")

            self.collapsed = False

            self.get_style_context().remove_class("collapsed")

    def on_update_segments(self, segments: List[List[str]]) -> None:
        if not self.updating_segment_list:
            for index, (segment_type_id, segment_id) in enumerate(segments):
                if segment_id in self.displayed_segments:
                    segment_container: SegmentContainer = self.displayed_segments[segment_id]

                    row: Gtk.ListBoxRow = segment_container.get_parent()

                    if row is not None:
                        row.remove(segment_container)

                        if row.get_parent() is not None:
                            row.get_parent().remove(row)

                else:
                    self.create_segment_container(segment_type_id, segment_id)

                self.displayed_segments[segment_id].set_scale(self.pdict.get("scale", 1.0))
                self.segment_list_box.insert(self.displayed_segments[segment_id], -1)

                self.add_toolbox(
                    self.displayed_segments[segment_id].segment.get_toolbox(), segment_id,
                )

                row: Gtk.ListBoxRow = self.segment_list_box.get_children()[-1]
                row.set_activatable(False)
                row.set_selectable(False)
                row.set_can_focus(False)

    def on_update_scale(self, scale: float):
        for id, segment_container in self.displayed_segments.items():
            segment_container.set_scale(scale)

    def on_update_archived(self, archived: bool):
        if archived:
            self.close()
        else:
            self.show()
            self.present()

    def on_update_window_size(self, window_size: List[int]) -> None:
        if (
            (not self.configuring)
            and (not self.content_revealer.get_reveal_child())
            and not self.collapsed
        ):
            self.resize(*window_size)

    def on_update_window_position(self, window_position: List[int]) -> None:
        if not self.configuring:
            self.move(*window_position)

    def on_update_window_maximized(self, window_maximized: bool) -> None:
        if not self.configuring:
            if window_maximized:
                self.maximize()
            else:
                self.unmaximize()
