"""
PersistentDict is a data structure similar to a dict (it uses dicts internally) thet saves to a file and loads from the file after an application restart.

You could (in theory) replace json with pickle or hjson or even


class storage:
    '''Simple json parser'''

    @staticmethod
    def load(file):
        return eval(file.read())

    @staticmethod
    def dump(obj, file):
        file.write(repr(obj))

FILE_EXTENSION = "ENDMYPAIN"
DECODE_EROOR = Exception


(please don't)
"""

import os.path

# define object notation system for dict storage
import json as storage

FILE_EXTENSION = "json"
DECODE_EROOR = storage.decoder.JSONDecodeError

# The file-readonly combinations that are already initialised and their objects.
# This reduces redundancy and data/listener inconsistencies to zero.
_KNOWN_DICTS = {}


class PersistentDict:
    """An object that provides an interface for an object notation (default: json) file (with listeners)."""

    _known_dicts = {}  # Keep instance reference

    def __new__(cls, file="settings." + FILE_EXTENSION, readonly=False):
        """Create a new PersistentDict or return an existing one that matches filename and read_only."""
        file = os.path.abspath(file)

        if (file, readonly) not in cls._known_dicts:
            cls._known_dicts[(file, readonly)] = object.__new__(cls)
        return cls._known_dicts[(file, readonly)]

    def __init__(self, file="settings." + FILE_EXTENSION, readonly=False):
        """Initialize a new PersistentDict object for a file."""
        if not hasattr(self, "initialized"):
            self.initialized = True

            self.metadata = {}

            self.file = file
            self.read = True
            self.reading = False
            self.readonly = readonly

            try:
                with open(self.file, "r") as settings_file:
                    self.settings_dict = storage.load(settings_file)
            except FileNotFoundError:
                self.settings_dict = {}
                self.write_settings()
            except DECODE_EROOR:
                self.settings_dict = {}
                self.write_settings()

            if os.path.isfile("settings_metadata." + FILE_EXTENSION):
                with open("settings_metadata." + FILE_EXTENSION, "r") as metadata_file:
                    self.metadata = storage.load(metadata_file)

            self.change_listeners = {key: [] for key in self.settings_dict}

            self.is_setting = {}

    def reset(self):
        """Reset all the settings."""
        self.settings_dict = {}
        self.write_settings()

    def read_settings(self):
        """Read the settings from the file and call listeners where necessary."""
        if not self.reading:
            self.reading = True
            try:
                with open(self.file, "r") as settings_file:
                    new_settings = storage.load(settings_file)
                for key in new_settings:
                    if key not in self.settings_dict:
                        self.change_listeners[key] = []
                    elif self.settings_dict[key] != new_settings[key]:
                        for change_listener in self.change_listeners[key]:
                            change_listener(new_settings[key])
                    self.settings_dict[key] = new_settings[key]

                self.reading = False
            except FileNotFoundError:
                self.write_settings()

    def write_settings(self):
        """Write the settings to the file."""
        if not self.readonly:
            with open(self.file, "w") as settings_file:
                storage.dump(self.settings_dict, settings_file)
        else:
            raise IOError("This PersistentDict is read-only")

    def __getitem__(self, key):
        """
        Get a key's value.

        This method accepts tuples so that you can call persistent_dict[key, default_value] to provide default values.
        Valid are:
            persistent_dict[key]
            persistent_dict[key, default_value]
            persistent_dict[key,]
        """
        if isinstance(key, tuple):
            if len(key) == 2:
                return self.get(key[0], key[1])
            elif len(key) == 1:
                return self.get(key[0])
            else:
                raise ValueError(
                    "PersistentDict.__getitem__() can handle key strings, tuples of length 1 (just a key string) and 2 (key string and default value)"
                )
        else:
            return self.get(key, None)

    def __setitem__(self, key, value):
        """Set the value for a key."""
        return self.set(key, value)

    def __delitem__(self, key):
        """Delete a key from the PersistentDict."""
        self.settings_dict.__delitem__(key, None)
        self.write_settings()

    # def __iter__(self):
    #     """Return an iterator for the PersistentDict"""
    #     return self.settings_dict.__iter__()

    def __contains__(self, key):
        """Return if the PersistentDict contains a key."""
        return self.settings_dict.__contains__(key)

    def __len__(self):
        """Return the amount of keys saved in the PersistentDict."""
        return self.settings_dict.__len__()

    def __copy__(self):
        """
        Raise an exception.

        PersistentDict does not support __copy__()
        """
        raise TypeError(
            "PersistentDict doesn't support __copy__() as that would lead to inconsistencies when writing/reading.\nIf you want a read-only version of this PersistentDict that always reflects the file this PersistentDict writes to (but does not provide reliable listeners) use __deepcopy__() or create a new PersistentDict pointing to the same file with the readonly argument."
        )

    def __deepcopy__(self):
        """Return a read-only version of the PersistentDict."""
        return PersistentDict(self.file, readonly=True)

    def __str__(self):
        """Format the PersistentDict in a string."""
        return "Persistentdict(" + str(dict(self.settings_dict)) + ")"

    def __iter__(self):
        """Return an iterator."""
        return self.settings_dict.__iter__()

    def items(self):
        """Return the items of the PersistentDict like so: [(k1, v1), (k2, v2), ...]."""
        return self.settings_dict.items()

    def get(self, key, default=None):
        """Get the value of a key in current settings. Set to default and return default if nonexistent."""
        if not isinstance(key, str):
            raise ValueError("Argument key must be a string.")

        self.read_settings()
        if key in self.settings_dict:
            return self.settings_dict[key]

        elif default is not None:
            self.set(key, default)
            return self.get(key)

        else:
            raise KeyError("Invalid settings key: " + str(key))

    def get_settings(self):
        """Return the whole dict of settings."""
        self.read_settings()
        return self.settings_dict

    def set(self, key, value):
        """Set the value of a key. Call all listeners added to the key (only if value is different to the key's previous value)."""
        if not isinstance(key, str):
            raise ValueError("Argument key must be a string.")

        if not self.readonly:
            was_setting = self.is_setting.get(key, False)

            self.is_setting[key] = True
            if key not in self.change_listeners:
                self.change_listeners[key] = []

            original_value = None

            if key in self.settings_dict:
                original_value = self.settings_dict[key]

                if not was_setting:
                    if value != original_value:
                        for change_listener in self.change_listeners[key]:
                            change_listener(value)

            if self.settings_dict.get(key, None) == original_value:
                self.settings_dict[key] = value

            self.write_settings()
            self.is_setting[key] = False
        else:
            raise IOError("This PersistentDict is read-only")

    def add_change_listener(self, key, listener):
        """
        Add a listener to a key to be called before the key's value changes with the new value as an argument

        The new value will NOT be returned when you call PersistentDict.get() as it is not updated there yet.
        """
        if not isinstance(key, str):
            raise ValueError("Argument key must be a string.")

        if not self.readonly:
            self.read_settings()
            if key in self.change_listeners:
                if listener not in self.change_listeners[key]:
                    self.change_listeners[key].append(listener)
            else:
                raise KeyError("Invalid settings key: " + str(key))
        else:
            raise IOError("This PersistentDict is read-only (no listeners either)")

    def remove_change_listener(self, key, listener):
        """
        Remove a listener from a key.

        Must be the exact same function/method.
        """
        if not isinstance(key, str):
            raise ValueError("Argument key must be a string.")

        if key in self.change_listeners:
            if listener in self.change_listeners[key]:
                self.change_listeners[key].remove(listener)
            else:
                raise ValueError(listener.name + " is not a change listener for " + str(key))
        else:
            raise KeyError("Invalid settings key: " + str(key))

    def get_metadata(self, key):
        """Return the metadata stored for the key or None.

        Metadata may look like one of the following:
            Ranges
                Range (int, step=1):        list        [start (int), end (int)]
                Range (int):                list        [start (int), end (int), step (int)]
                Range (float, step=1):      list        [start (int / float), end (int / float)]
                Range (float):              list        [start (int / float), end (int / float), step (int / float)]
            Choices
                Choice                      list        [option1, option2, ...]
            Type override
                Type override               string      typename
                                                        May be one of "str", "int", "float", "list", "bool"
        """
        if not isinstance(key, str):
            raise ValueError("Argument key must be a string.")

        if key in self.metadata:
            return self.metadata[key]
        return None

    def set_metadata(self, key, metadata):
        """Set the metadata stored for the key.

        Metadata may look like one of the following:
            Ranges
                Range (int, step=1):        list        [start (int), end (int)]
                Range (int):                list        [start (int), end (int), step (int)]
                Range (float, step=1):      list        [start (int / float), end (int / float)]
                Range (float):              list        [start (int / float), end (int / float), step (int / float)]
            Choices
                Choice                      list        [option1, option2, ...]
            Type override
                Type override               string      typename
                                                        May be one of "str", "int", "float", "list", "bool"
        """
        if not isinstance(key, str):
            raise ValueError("Argument key must be a string.")

        self.metadata[key] = metadata

    def export(self, path, exclude_keys=[]):
        export_dict = self.settings_dict

        for key in exclude_keys:
            if key in export_dict:
                del export_dict[key]

        with open(path, "w") as settings_file:
            storage.dump(export_dict, settings_file)

    def load(self, path):
        self.reading = True
        with open(path, "r") as settings_file:
            new_settings = storage.load(settings_file)
        for key in new_settings:
            if key not in self.settings_dict:
                self.change_listeners[key] = []
            elif self.settings_dict[key] != new_settings[key]:
                for change_listener in self.change_listeners[key]:
                    change_listener(new_settings[key])
            self.settings_dict[key] = new_settings[key]

        self.reading = False


if __name__ == "__main__":
    pd = PersistentDict("test_peristentdict." + FILE_EXTENSION)
    print(pd)
    pd["test_key"] = not pd["test_key", False]
    print(pd)
