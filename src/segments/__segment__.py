"""An abstract Segment class that all segments inherit from."""

from abc import ABC, abstractproperty, abstractmethod
from typing import TypedDict, Optional, Union, Dict, Callable, Any

from ..persistentdict import PersistentDict

from gi.repository import Gio, Gtk


class Segment(ABC):
    """A segment is a section of a note in INA. It may contain any kind of (editable) content."""

    @abstractmethod
    def __init__(self, pdict: PersistentDict):
        """Create a new Segment.

        You should create instance-specific stuff here,
            e.g. the widget that content() returns and the toolbox (returned by toolbox())

        If you want to leave this blank, just overwrite it with "def __init__(self): ..."
        """
        ...

    @abstractproperty
    def name(self) -> Union[str, Dict[str, str]]:
        """
        The name of the segment type.

        Should be str or a dict with locales (e.g. "de", en", "en_GB") and the corresponding
        translations of the tool name.

        The default locale (used when no other locale fits) is "C".
        """
        ...

    @abstractproperty
    def icon(self) -> Gio.Icon:
        """
        An icon that represents the section type.

        E.g. Gio.ThemedIcon.new("font-x-generic-symbolic")

        Using a symbolic icon (ie. dynamic colour) is recommended.
        """
        ...

    @abstractmethod
    def get_content(self) -> Gtk.Widget:
        """
        Return the Gtk.Widget that is added to the segment's content area.

        This will be resized on hover and focus to make room for the control area.
        """
        ...

    @abstractmethod
    def get_toolbox(self) -> Optional[Gtk.Widget]:
        """
        Return a widget that shows tools to the user or None to not show one.

        You should create a new instance every time this gets called as it might be
            added to multiple windows.

        When this is called, you can safely assume that the content returned by get_content() has
            been added to a toplevel. Therefore you could add accelerator groups to the note with
            Gtk.Window.add_accel_group() and give your tool items accelerators (highly recommended).

        It is best practice to add a tooltip to your tool items that include their function and the
            accelerator, I recommend the format "Function (<i>Accel+E</i>)" (with markup).
        """
        ...

    @abstractmethod
    def get_pdict(self) -> PersistentDict:
        """
        Return a PersistentDict that can be used to restore the segment.
        """
        ...

    @abstractmethod
    def set_scale(self, scale: float) -> None:
        """
        Set the UI scale of self.

        The widget will be given enough space to be scaled directly,
            e.g. from 500px to 750px with scale 1.5.

        Args:
            scale (float): The factor by which to scale. May be below 1.
        """

    def focus(self) -> None:
        """
        Focus the default widget of the segment.

        This might happen if the note is re-opened or the segment is newly created.
        """
        print(self.get_content().get_focus_chain())
        if (
            isinstance(self.get_content(), Gtk.Container)
            and self.get_content().get_focus_child() is not None
        ):
            self.get_content().get_focus_child().grab_focus()
        else:
            self.get_content().grab_focus()
