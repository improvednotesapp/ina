from ...__segment__ import Segment
from ....persistentdict import PersistentDict
from typing import Union, Tuple, List

from gi.repository import GLib, Gio, Pango, Gdk, Gtk

import time

test_label = Gtk.Label()
print(test_label.get_style_context().get_font(Gtk.StateFlags.NORMAL).to_string())
test_label.get_style_context().add_class("monospace")
print(test_label.get_style_context().get_font(Gtk.StateFlags.NORMAL).to_string())


@Gtk.Template(resource_path="/org/czarlie/Ina/Segments/ina_text.ui")
class TextWidget(Gtk.Bin):
    __gtype_name__ = "TextWidget"

    textview: Gtk.TextView = Gtk.Template.Child()
    buffer: Gtk.TextBuffer = Gtk.Template.Child()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.textview.drag_dest_unset()

        self.connect("size-allocate", self.on_size_allocate)
        self.buffer.connect("mark-set", self.on_mark_set)

    def on_size_allocate(
        self, widget: Gtk.Widget, allocation: Gdk.Rectangle
    ) -> None:
        # Work around TextView not wanting to give up its size
        # This has to be called multiple times though, hence the repeated calls of queue_resize()
        # FIXME: Will crash in larger sizes with longer texts
        allocation.width = self.get_parent().get_size_request()[0]
        self.textview.size_allocate(allocation)

    def on_mark_set(self, *args):
        scrolledwindow: Gtk.ScrolledWindow = self.get_ancestor(
            Gtk.ScrolledWindow
        )
        segment_list: Gtk.ListBox = self.get_ancestor(Gtk.ListBox)

        if scrolledwindow is not None:
            vadj: Gtk.Adjustment = scrolledwindow.get_vadjustment()

            cursor_locations: Tuple[
                Gdk.Rectangle, Gdk.Rectangle
            ] = self.textview.get_cursor_locations()

            for cursor_location in cursor_locations:
                dest_min_y = self.textview.translate_coordinates(
                    segment_list,
                    *self.textview.buffer_to_window_coords(
                        Gtk.TextWindowType.WIDGET,
                        0,
                        cursor_location.y - cursor_location.height,
                    ),
                )[1]
                dest_max_y = self.textview.translate_coordinates(
                    segment_list,
                    *self.textview.buffer_to_window_coords(
                        Gtk.TextWindowType.WIDGET,
                        0,
                        cursor_location.y + 2 * cursor_location.height,
                    ),
                )[1]

                vadj.props.value = max(
                    vadj.props.value,
                    dest_max_y - scrolledwindow.get_allocated_height(),
                )
                vadj.props.value = min(vadj.props.value, dest_min_y)


class TextSegment(Segment):
    name = "Text"
    icon = Gio.ThemedIcon.new("font-x-generic-symbolic")

    def __init__(self, pdict):
        self.pdict: PersistentDict = pdict

        self.css_provider = Gtk.CssProvider()
        self.css_provider.load_from_data(b"*{font-size:100%}")

        self.content: TextWidget = TextWidget()

        self.content.buffer.connect("changed", self.on_content_changed)
        self.content.get_style_context().add_provider(
            self.css_provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        )
        self.content.textview.get_style_context().add_provider(
            self.css_provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        )

        self.undoing: bool = False

        self.undo_history: List[bytes] = []
        self.redo_history: List[bytes] = []

        self.bold_tag = self.content.buffer.create_tag("bold", weight=Pango.Weight.BOLD)
        self.italic_tag = self.content.buffer.create_tag(
            "style", style=Pango.Style.ITALIC
        )
        self.underline_tag = self.content.buffer.create_tag(
            "underline", underline=Pango.Underline.SINGLE
        )
        self.strikethrough_tag = self.content.buffer.create_tag(
            "strikethrough", strikethrough=True
        )
        self.monospace_tag = self.content.buffer.create_tag(
            "monospace", family="monospace"
        )

        self.content.buffer.deserialize(
            self.content.buffer,
            self.content.buffer.register_deserialize_tagset(),
            self.content.buffer.get_start_iter(),
            bytes.fromhex(
                self.pdict.get(
                    "rich_text",
                    self.content.buffer.serialize(
                        self.content.buffer,
                        self.content.buffer.register_serialize_tagset(),
                        *self.content.buffer.get_bounds(),
                    ).hex(),
                )
            ),
        )

        self.last_state_before_history_update = bytes.fromhex(
            self.pdict.get("rich_text")
        )

        self.on_update_align(self.pdict.get("align", "left"))
        self.pdict.add_change_listener("align", self.on_update_align)

        self.content.show_all()

        self.last_sizes = []

        self.BUTTONS = (
            {
                "type": "button",
                "markup": "<b>Bb</b>",
                "callback": self.on_bold_clicked,
                "accel": "<Primary>b",
                "pack": "start",
                "label": "Bold",
            },
            {
                "type": "button",
                "markup": "<i>Ii</i>",
                "callback": self.on_italic_clicked,
                "accel": "<Primary>i",
                "pack": "start",
                "label": "Italic",
            },
            {
                "type": "button",
                "markup": "<u>Uu</u>",
                "callback": self.on_underline_clicked,
                "accel": "<Primary>u",
                "pack": "start",
                "label": "Underline",
            },
            {
                "type": "button",
                "markup": "<s>Ss</s>",
                "callback": self.on_strikethrough_clicked,
                "accel": "<Primary><Shift>s",
                "pack": "start",
                "label": "Strikethrough",
            },
            {
                "type": "button",
                "markup": "<tt>Mm</tt>",
                "callback": self.on_monospace_clicked,
                "accel": "<Primary>m",
                "pack": "start",
                "label": "Monospace",
            },
            {"type": "separator", "pack": "start"},
            {
                "type": "button",
                "icon": "format-justify-left-symbolic",
                "callback": self.on_left_align_clicked,
                "accel": "<Primary><Shift>l",
                "pack": "start",
                "label": "Align Left",
            },
            {
                "type": "button",
                "icon": "format-justify-center-symbolic",
                "callback": self.on_center_align_clicked,
                "accel": "<Primary><Shift>c",
                "pack": "start",
                "label": "Align Center",
            },
            {
                "type": "button",
                "icon": "format-justify-right-symbolic",
                "callback": self.on_right_align_clicked,
                "accel": "<Primary><Shift>r",
                "pack": "start",
                "label": "Align Right",
            },
            {
                "type": "button",
                "icon": "format-justify-fill-symbolic",
                "callback": self.on_fill_align_clicked,
                "accel": "<Primary><Shift>f",
                "pack": "start",
                "label": "Justify",
            },
            {"type": "separator", "pack": "start"},
            {
                "type": "button",
                "icon": "edit-redo-symbolic",
                "callback": self.on_redo_clicked,
                "accel": "<Primary>y",
                "pack": "end",
                "label": "Redo",
            },
            {
                "type": "button",
                "icon": "edit-undo-symbolic",
                "callback": self.on_undo_clicked,
                "accel": "<Primary>z",
                "pack": "end",
                "label": "Undo",
            },
        )

    def get_content(self):
        return self.content

    def get_toolbox(self, buttons=None, orientation=Gtk.Orientation.VERTICAL):
        toolbox: Gtk.Box = Gtk.Box(orientation=orientation)

        accel_group = Gtk.AccelGroup()
        self.content.get_toplevel().add_accel_group(accel_group)

        size_group = Gtk.SizeGroup()
        size_group.set_mode(Gtk.SizeGroupMode.BOTH)

        if buttons is None:
            buttons = self.BUTTONS

        for button_dict in buttons:
            padding = 0

            if button_dict["type"] == "button":
                if "markup" in button_dict:
                    button = Gtk.Button()
                    label = Gtk.Label()
                    label.set_markup(button_dict["markup"])
                    button.add(label)

                elif "icon" in button_dict:
                    button = Gtk.Button.new_from_icon_name(
                        button_dict["icon"], Gtk.IconSize.SMALL_TOOLBAR
                    )

                button.connect("clicked", button_dict["callback"])

                accel = Gtk.accelerator_parse(button_dict["accel"])

                button.add_accelerator(
                    "clicked", accel_group, *accel, Gtk.AccelFlags.VISIBLE,
                )
                button.set_tooltip_markup(
                    f'{button_dict["label"]} '
                    f'<span style="italic" alpha="50%">'
                    f"{Gtk.accelerator_get_label(*accel)}"
                    f"</span>"
                )

                size_group.add_widget(button)

            elif button_dict["type"] == "separator":
                button = Gtk.Separator(orientation=Gtk.Orientation.HORIZONTAL)
                padding = 4

            else:
                raise ValueError(f'Invalid button type: {button_dict["type"]}')

            if button_dict["pack"] == "start":
                toolbox.pack_start(button, fill=False, expand=False, padding=padding)

            else:
                toolbox.pack_end(button, fill=False, expand=False, padding=padding)

        return toolbox

    def get_pdict(self):
        return self.pdict

    def focus(self):
        self.content.textview.grab_focus()

    def set_scale(self, scale):
        self.scale = scale

        if self.content.get_parent() is not None:
            self.content.set_size_request(
                self.content.get_parent().get_size_request()[0], -1
            )

            self.css_provider.load_from_data(
                "*{{font-size:{}%}}".format(scale * 100).encode()
            )

        # This works around weird TextView resize behaviour.
        self.last_sizes = []
        self.resize_perpetually(scale=scale)

    def on_content_changed(self, *args):
        serialized = self.content.buffer.serialize(
            self.content.buffer,
            self.content.buffer.register_serialize_tagset(),
            *self.content.buffer.get_bounds(),
        )

        if not self.undoing:
            self.redo_history = []
            step = bytes.fromhex(self.pdict.get("rich_text", serialized.hex()))
            self.wait_append_undo_step(step)

        self.pdict.set("rich_text", serialized.hex())

    def wait_append_undo_step(self, step: bytes):
        GROUP_TIME: int = 200

        self.last_modified = time.time() * 1000

        def append_undo_step():
            if time.time() * 1000 - self.last_modified >= GROUP_TIME:
                if (not len(self.undo_history)) or self.undo_history[-1] != step:
                    self.undo_history.append(self.last_state_before_history_update)

                    self.last_state_before_history_update = bytes.fromhex(
                        self.pdict.get("rich_text")
                    )

        GLib.timeout_add(GROUP_TIME, append_undo_step)

    def resize_perpetually(self, delay: int = 75, scale: float = -1):
        if scale == -1 or scale == self.scale:
            self.content.queue_resize()

            alloc = self.content.get_allocation()
            size = alloc.width, alloc.height

            self.last_sizes.append(size)
            self.last_sizes = self.last_sizes[-10:]

            if self.last_sizes != [size] * 10:
                GLib.timeout_add(delay, self.resize_perpetually, delay)

    def add_tag(
        self, start_iter: Gtk.TextIter, end_iter: Gtk.TextIter, tag: Gtk.TextTag,
    ):
        self.content.buffer.apply_tag(tag, start_iter, end_iter)
        self.on_content_changed()

    def remove_tag(
        self, start_iter: Gtk.TextIter, end_iter: Gtk.TextIter, tag: Gtk.TextTag,
    ):
        self.content.buffer.remove_tag(tag, start_iter, end_iter)
        self.on_content_changed()

    def toggle_tag(
        self, start_iter: Gtk.TextIter, end_iter: Gtk.TextIter, tag: Gtk.TextTag,
    ):
        i: Gtk.TextIter = start_iter.copy()

        unboldified = False
        while not i.equal(end_iter):
            tags: List[Gtk.TextTag] = i.get_tags()

            for iter_tag in tags:
                if iter_tag is tag:
                    self.remove_tag(start_iter, end_iter, tag)
                    unboldified = True

            i.forward_char()

        if not unboldified:
            self.add_tag(start_iter, end_iter, tag)

    def on_bold_clicked(self, button: Gtk.Button):
        self.on_format_clicked(self.bold_tag)

    def on_italic_clicked(self, button: Gtk.Button):
        self.on_format_clicked(self.italic_tag)

    def on_underline_clicked(self, button: Gtk.Button):
        self.on_format_clicked(self.underline_tag)

    def on_strikethrough_clicked(self, button: Gtk.Button):
        self.on_format_clicked(self.strikethrough_tag)

    def on_monospace_clicked(self, button: Gtk.Button):
        self.on_format_clicked(self.monospace_tag)

    def on_format_clicked(self, tag: Gtk.TextTag):
        selection_bounds: Union[
            Tuple[Gtk.TextIter, Gtk.TextIter], Tuple
        ] = self.content.buffer.get_selection_bounds()

        if len(selection_bounds) == 2:
            selection_bounds[0].order(selection_bounds[1])
            self.toggle_tag(*selection_bounds, tag)

        else:
            cursor = self.content.buffer.get_iter_at_mark(
                self.content.buffer.get_mark("insert")
            )

            start_iter = cursor.copy()
            if not start_iter.starts_word():
                start_iter.backward_word_start()

            end_iter = cursor.copy()
            if not end_iter.ends_word():
                end_iter.forward_word_end()

            self.toggle_tag(start_iter, end_iter, tag)

        self.content.textview.grab_focus()

    def on_left_align_clicked(self, button: Gtk.Button):
        self.pdict.set("align", "left")

    def on_center_align_clicked(self, button: Gtk.Button):
        self.pdict.set("align", "center")

    def on_right_align_clicked(self, button: Gtk.Button):
        self.pdict.set("align", "right")

    def on_fill_align_clicked(self, button: Gtk.Button):
        self.pdict.set("align", "fill")

    def on_undo_clicked(self, button: Gtk.Button):
        self.undoing = True
        if len(self.undo_history):
            undo_step = self.undo_history.pop()

            redo_step = self.content.buffer.serialize(
                self.content.buffer,
                self.content.buffer.register_serialize_tagset(),
                *self.content.buffer.get_bounds(),
            )

            self.content.buffer.delete(*self.content.buffer.get_bounds())

            self.content.buffer.deserialize(
                self.content.buffer,
                self.content.buffer.register_deserialize_tagset(),
                self.content.buffer.get_start_iter(),
                undo_step,
            )
            self.redo_history.append(redo_step)
        self.undoing = False

    def on_redo_clicked(self, button: Gtk.Button):
        self.undoing = True
        if len(self.redo_history):
            redo_step = self.redo_history.pop()

            undo_step = self.content.buffer.serialize(
                self.content.buffer,
                self.content.buffer.register_serialize_tagset(),
                *self.content.buffer.get_bounds(),
            )

            self.content.buffer.delete(*self.content.buffer.get_bounds())

            self.content.buffer.deserialize(
                self.content.buffer,
                self.content.buffer.register_deserialize_tagset(),
                self.content.buffer.get_start_iter(),
                redo_step,
            )
            self.undo_history.append(undo_step)
        self.undoing = False

    def on_update_align(self, align: str):
        if align == "left":
            self.content.textview.set_justification(Gtk.Justification.LEFT)
        elif align == "center":
            self.content.textview.set_justification(Gtk.Justification.CENTER)
        elif align == "right":
            self.content.textview.set_justification(Gtk.Justification.RIGHT)
        elif align == "fill":
            self.content.textview.set_justification(Gtk.Justification.FILL)
        else:
            self.pdict.set("align", "left")
