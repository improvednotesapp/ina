# window.py
#
# Copyright 2020 Czarlie
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE X CONSORTIUM BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
# Except as contained in this notice, the name(s) of the above copyright
# holders shall not be used in advertising or otherwise to promote the sale,
# use or other dealings in this Software without prior written
# authorization.

from time import time

from gi.repository import GLib, Gdk, Gtk

import cairo

from .segments.__segment__ import Segment
from .persistentdict import PersistentDict
from . import colors

STYLE = """
@define-color segment-bg rgb{bg};
@define-color segment-contrast rgb{contrast};
@define-color segment-contrast-contrast rgb{ccontrast};
"""

INA_SEGMENT_ATOM = Gdk.Atom.intern("INA_SEGMENT", False)
DND_ENTRIES = [Gtk.TargetEntry.new("INA_SEGMENT", Gtk.TargetFlags.SAME_APP, 0)]

HANDLES = {}


@Gtk.Template(resource_path="/org/czarlie/Ina/segment.ui")
class SegmentContainer(Gtk.EventBox):
    __gtype_name__ = "SegmentContainer"

    content_area: Gtk.Box = Gtk.Template.Child()

    segment_control_revealer: Gtk.Revealer = Gtk.Template.Child()
    control_area: Gtk.Box = Gtk.Template.Child()

    more_button: Gtk.Button = Gtk.Template.Child()

    color_chooser: Gtk.ColorChooser = Gtk.Template.Child()
    color_chooser_button: Gtk.Button = Gtk.Template.Child()
    color_chooser_popover: Gtk.Popover = Gtk.Template.Child()

    drag_handle_image: Gtk.Image = Gtk.Template.Child()
    drag_handle: Gtk.EventBox = Gtk.Template.Child()

    def __init__(self, segment: Segment, note: Gtk.Window, **kwargs):
        super().__init__(**kwargs)
        self.set_events(Gdk.EventMask.POINTER_MOTION_MASK)

        self.app_pdict = PersistentDict("app.json")
        self.note_pdict = note.pdict

        self.pointer_inside = False
        self.pointer_last_inside_time = 0

        self.segment: Segment = segment

        self.content = segment.get_content()
        self.content_area.pack_start(self.content, fill=True, expand=True, padding=0)

        HANDLES[self.segment.get_pdict().get("id")] = self.drag_handle

        self.init_dnd()
        self.init_color_picker()
        self.set_scale(1.0)

        menu_builder = Gtk.Builder.new_from_resource(
            "/org/czarlie/Ina/segment_menu.xml"
        )
        self.menu = Gtk.Menu.new_from_model(menu_builder.get_object("segment_menu"))

        self.css_provider = Gtk.CssProvider()
        self.add_style(self)
        self.add_style(self.color_chooser_popover)

        self.connect_signals()
        self.add_listeners()

    def add_listeners(self):
        self.on_update_background(
            self.segment.get_pdict().get(
                "background",
                self.app_pdict.get("default_segment_background", "#ace5d4"),
            )
        )
        self.segment.get_pdict().add_change_listener(
            "background", self.on_update_background
        )

        self.app_pdict.get("segment_width", 450)
        self.app_pdict.add_change_listener(
            "segment_width", self.on_update_segment_width
        )

    def init_dnd(self):
        self.drag_handle.drag_source_set(
            Gdk.ModifierType.BUTTON1_MASK, DND_ENTRIES, Gdk.DragAction.MOVE,
        )
        self.drag_handle.connect("drag-data-get", self.on_drag_data_get)
        self.drag_handle.connect("drag-begin", self.on_drag_begin)
        self.drag_handle.connect("drag-end", self.on_drag_end)
        self.drag_handle.connect("drag-failed", self.on_drag_failed)

    def init_color_picker(self):
        color_choices = []
        hex_colors = self.app_pdict.get(
            "default_color_choices",
            [
                *["#d2e5b7", "#e5e2b7", "#e5cfb7", "#e5b7b7", "#e5c3e0"],
                *["#a4a0e5", "#85bae5", "#ace5d4", "#333333", "#eeeeee"],
                *["#94b26b", "#ccc67a", "#b2916b", "#b26b6b", "#b26ba8"],
                *["#6d6bb2", "#6b92b2", "#6bb29d", "#555555", "#cccccc"],
                *["#2D3324", "#333224", "#332B24", "#332424", "#332431"],
                *["#2D2C4C", "#2E3F4C", "#2D403A", "#777777", "#aaaaaa"],
            ],
        )

        for hex_color in hex_colors:
            rgba = Gdk.RGBA()
            rgba.parse(hex_color)
            color_choices.append(rgba)

        self.color_chooser.add_palette(Gtk.Orientation.HORIZONTAL, 10, color_choices)
        self.color_chooser_visible: bool = False

    def add_style(self, widget: Gtk.Widget):
        widget.get_style_context().add_provider(
            self.css_provider, Gtk.STYLE_PROVIDER_PRIORITY_SETTINGS
        )

        if isinstance(widget, Gtk.Container):
            for child in widget.get_children():
                self.add_style(child)

    def set_scale(self, scale: float):
        self.content_area.set_size_request(
            min(
                round(self.app_pdict.get("segment_width", 450) * scale),
                self.app_pdict.get("max_segment_width", 1000),
            ),
            -1,
        )
        self.segment.set_scale(scale)

    def on_update_segment_width(self, segment_width):
        self.content_area.set_size_request(
            min(
                round(segment_width * self.note_pdict.get("scale", 1.0)),
                self.app_pdict.get("max_segment_width", 1000),
            ),
            -1,
        )
        self.segment.set_scale(self.note_pdict.get("scale", 1.0))

    def on_update_background(self, background: str):
        bg = colors.string_to_rgb255(background)
        contrast = colors.contrast(
            bg,
            *[
                colors.string_to_rgb255(color)
                for color in self.app_pdict.get(
                    "foreground_color_options", ["#222222", "#ffffff"]
                )
            ]
        )
        ccontrast = colors.contrast(
            contrast,
            *[
                colors.string_to_rgb255(color)
                for color in self.app_pdict.get(
                    "foreground_color_options", ["#222222", "#ffffff"]
                )
            ]
        )
        self.css_provider.load_from_data(
            STYLE.format(bg=bg, contrast=contrast, ccontrast=ccontrast).encode("utf-8")
        )

        rgba = Gdk.RGBA()
        rgba.parse(background)
        self.color_chooser.set_rgba(rgba)

    def update_button_visibility(self):
        if (
            (not self.pointer_inside)
            and time() - self.pointer_last_inside_time
            >= self.app_pdict.get("segment_button_hide_delay", 0.0)
            and not self.color_chooser_visible
        ):
            toplevel = self.get_toplevel()

            if toplevel is None or not hasattr(toplevel, "get_focus"):
                return

            focus_widget = toplevel.get_focus()

            while (
                focus_widget is not toplevel
                and focus_widget is not self
                and focus_widget is not None
            ):
                focus_widget = focus_widget.get_parent()

            if focus_widget is not self:
                self.segment_control_revealer.set_reveal_child(False)
        else:
            GLib.timeout_add_seconds(
                1, self.update_button_visibility,
            )

    def connect_signals(self):
        self.more_button.connect("clicked", self.on_more_clicked)

        self.color_chooser_button.connect(
            "clicked", self.on_color_chooser_button_clicked
        )
        self.color_chooser_popover.connect("hide", self.on_color_chooser_popover_hide)
        self.color_chooser.connect("notify::rgba", self.on_color_pick)

        self.color_chooser.connect("color-activated", self.on_color_activated)

    def focus_content(self):
        self.segment.focus()

    def do_motion_notify_event(self, *args):
        self.segment_control_revealer.set_reveal_child(True)

        self.pointer_inside = True

    def do_enter_notify_event(self, *args):
        self.segment_control_revealer.set_reveal_child(True)

        self.pointer_inside = True

    def do_leave_notify_event(self, *args):
        self.pointer_last_inside_time = time()
        self.pointer_inside = False

        GLib.timeout_add(
            self.app_pdict.get("segment_button_hide_delay", 0.0) * 1000,
            self.update_button_visibility,
        )

    def on_window_set_focus(self, window, focus_widget):
        GLib.timeout_add(
            self.app_pdict.get("segment_button_hide_delay", 0.0) * 1000,
            self.update_button_visibility,
        )

    def on_drag_data_get(
        self,
        widget: Gtk.Widget,
        context: Gdk.DragContext,
        data: Gtk.SelectionData,
        info: int,
        time: int,
    ):
        data.set(
            Gdk.Atom.intern("INA_SEGMENT", True),
            32,
            self.segment.get_pdict().get("id").encode(),
        )

    def on_drag_begin(
        self, widget: Gtk.Widget, context: Gdk.DragContext,
    ):
        row = self.get_parent()
        alloc = row.get_allocation()
        surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, alloc.width, alloc.height)
        cr = cairo.Context(surface)
        row.draw(cr)

        Gtk.drag_set_icon_surface(context, surface)
        context.set_hotspot(*row.get_pointer())  # deprecated, grrr...

        self.set_opacity(0)

    def on_drag_end(
        self, widget: Gtk.Widget, context: Gdk.DragContext,
    ):
        self.set_opacity(1)

        for handle in HANDLES.values():
            ancestor = handle.get_ancestor(SegmentContainer)

            if ancestor is not None:
                ancestor.unset_state_flags(Gtk.StateFlags.DROP_ACTIVE)
                ancestor.get_child().get_style_context().remove_class("drop-above")
                ancestor.get_child().get_style_context().remove_class("drop-below")

    def on_drag_failed(
        self, widget: Gtk.Widget, context: Gdk.DragContext, result: Gtk.DragResult
    ):
        toplevel: Gtk.Window = self.get_toplevel()
        note_window: Gtk.Window = toplevel.get_application().show_note()

        row = self.get_parent()
        row.get_parent().remove(row)
        row.remove(self)

        note_window.segment_list_box.add(self)
        note_window.add_toolbox(
            self.segment.get_toolbox(), self.segment.get_pdict().get("id")
        )

        row: Gtk.ListBoxRow = note_window.segment_list_box.get_children()[-1]
        row.set_activatable(False)
        row.set_selectable(False)
        row.set_can_focus(False)

        note_window.update_segment_list()
        toplevel.update_segment_list()

        drag_window = context.get_drag_window()
        rx, ry = drag_window.get_position()

        note_window.move(rx, ry)

        note_window.pdict.set("scale", toplevel.pdict.get("scale", 1.0))
        note_window.pdict.set(
            "background",
            toplevel.pdict.get(
                "background",
                toplevel.get_application().pdict.get("default_note_background", "#333"),
            ),
        )

        if (
            toplevel.pdict.get("title") in {"Untitled note", ""}
            and len(toplevel.pdict.get("segments")) == 0
        ):
            toplevel.force_delete()

        return True

    def on_more_clicked(self, *args):
        self.focus_content()
        self.menu.popup_at_widget(
            self.more_button, Gdk.Gravity.SOUTH_EAST, Gdk.Gravity.SOUTH_EAST, None
        )

    def on_color_chooser_button_clicked(self, *args):
        self.color_chooser_visible = True
        self.color_chooser.set_property("show-editor", False)

    def on_color_chooser_popover_hide(self, *args):
        self.color_chooser_visible = False

    def on_color_pick(self, *args):
        rgba: Gdk.RBGA = self.color_chooser.get_rgba()
        self.segment.get_pdict().set(
            "background",
            colors.rgb_to_hex(
                colors.rgb1a_to_rgb255((rgba.red, rgba.green, rgba.blue))
            ),
        )

    def on_color_activated(self, *args):
        self.color_chooser_popover.hide()
