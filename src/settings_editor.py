"""The dialog that is opened to edit settings."""

import os
import sys

import hjson

from fuzzywuzzy import fuzz

import string

import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk, GLib

if __package__ is not None and __package__ != "":
    from . import colors
else:
    import colors

EXCLUDE_SETTINGS = [
    "application_padding",
    "custom_font_family",  # setting has been removed
    "custom_font_size",  # setting has been removed
    "debug",
    "deleted",
    "formatting",
    "hidden",
    "maximized",
    "preferences_window_maximized",
    "preferences_window_size",
    "text",
    "title",
]

if sys.platform.startswith("win"):
    EXCLUDE_SETTINGS += [
        "middle_mouse_move",
        "middle_mouse_move_prompt",
    ]

TYPES = {
    "str": str,
    "int": int,
    "float": float,
    "list": list,
    "bool": bool,
    "font": "FONT",
}

languages = GLib.get_language_names()  # None-terminated

descriptions = {}
if os.path.isdir("descriptions"):
    locale_files = os.listdir("descriptions")
    for language in languages[::-1]:
        if language is not None:
            if language + ".hjson" in locale_files:
                with open(
                    "descriptions" + os.sep + language + ".hjson", "r"
                ) as descriptions_file:
                    descriptions.update(hjson.load(descriptions_file))

translations = {}
if os.path.isdir("translations"):
    locale_files = os.listdir("translations")
    for language in languages[::-1]:
        if language is not None:
            if language + ".hjson" in locale_files:
                with open(
                    "translations" + os.sep + language + ".hjson", "r"
                ) as descriptions_file:
                    translations.update(hjson.load(descriptions_file))


def key_to_human(key):
    if key in translations:
        key = translations[key]
    if "_" in key:
        return key.replace("_", " ").title()
    return key[0].upper() + key[1:]


class SettingsEditorWindow(Gtk.ApplicationWindow):
    def __init__(self, settings, parent=None):
        super().__init__()

        if parent is not None:
            self.set_transient_for(parent)

        self.settings = settings

        self.init()

    def init(self):
        self.settings_widgets = {}

        self.label_sizegroup = Gtk.SizeGroup(mode=Gtk.SizeGroupMode.HORIZONTAL)
        self.settings_widget_size_group = Gtk.SizeGroup(
            mode=Gtk.SizeGroupMode.HORIZONTAL
        )

        self.configure()

        self.layout()

        self.bind_events()

        self.show_all()

    def configure(self):
        """Set all relevant attributes to the window."""
        self.unmaximize()
        self.set_default_size(*self.settings.get("preferences_window_size", [400, 500]))
        if self.settings.get("preferences_window_maximized", False):
            self.maximize()

        if "title" in self.settings.get_settings():
            self.set_title("UNA Settings - Note Settings")
        elif self.settings.file == "settings.hjson":
            self.set_title("UNA Settings - App Settings")
        else:
            self.set_title(
                "UNA Plugin Settings - "
                + key_to_human(
                    self.settings.file.split(".hjson", 1)[0].split(os.sep)[-1]
                )
            )

    def layout(self):
        """Add all necessary sub-widgets to the widget."""
        self.main_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)

        self.search_entry_revealer = Gtk.Revealer()
        self.search_entry_revealer.set_border_width(5)
        self.search_entry_revealer.set_transition_type(
            Gtk.RevealerTransitionType.SLIDE_DOWN
        )

        self.search_entry_box = Gtk.Box()
        self.search_entry_box.set_border_width(5)

        self.search_entry = Gtk.Entry()
        self.search_entry.set_icon_from_icon_name(
            Gtk.EntryIconPosition.PRIMARY, "system-search-symbolic"
        )

        self.search_entry_box.pack_start(self.search_entry, True, True, 0)

        self.search_entry_revealer.add(self.search_entry_box)

        self.main_box.pack_start(self.search_entry_revealer, False, False, 0)

        self.scrolled_window = Gtk.ScrolledWindow()
        self.scrolled_window.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)

        self.box = Gtk.FlowBox(orientation=Gtk.Orientation.HORIZONTAL)
        self.box.set_border_width(self.settings.get("application_padding", 5) * 2)
        self.box.set_homogeneous(False)
        self.box.set_max_children_per_line(1)
        self.box.set_row_spacing(10)
        self.box.set_border_width(0)
        self.box.set_selection_mode(Gtk.SelectionMode.NONE)
        self.box.set_valign(Gtk.Align.START)
        self.box.set_sort_func(self.settings_sort_func)
        self.box.set_filter_func(self.settings_filter_func)

        SETTINGS = list(self.settings.get_settings().keys())
        SETTINGS.sort()
        for i in SETTINGS:
            if i not in EXCLUDE_SETTINGS:
                self.add_to_box(i)

        self.reset_button = Gtk.Button(label=key_to_human("reset_settings"))
        self.box.add(self.reset_button)
        self.reset_button.connect("clicked", self.on_reset_clicked)

        self.scrolled_window.add(self.box)
        self.main_box.pack_start(self.scrolled_window, True, True, 0)
        self.add(self.main_box)

    def add_to_box(self, key):
        self.settings_widgets[key] = SettingsWidget(
            self.settings, key, self.settings_widget_size_group
        )
        self.settings_widgets[key].set_halign(Gtk.Align.FILL)
        self.box.add(self.settings_widgets[key])

    def bind_events(self):
        """Bind Gtk events."""
        self.connect("configure-event", self.on_configure)
        self.connect_after("key-press-event", self.on_key_press)
        self.search_entry.connect("changed", self.on_search_changed)

    def on_configure(self, widget, event):
        self.settings.set("preferences_window_size", self.get_size())
        self.settings.set("preferences_window_maximized", self.is_maximized())

    def on_reset_clicked(self, button):
        self.settings.reset()
        msg_dialog = Gtk.MessageDialog(
            self,
            0,
            Gtk.MessageType.INFO,
            Gtk.ButtonsType.OK,
            text="Settings deleted. Restart application to set to default values.",
        )
        msg_dialog.run()
        msg_dialog.destroy()
        self.destroy()

    def settings_sort_func(self, settings_widget1, settings_widget2):
        if isinstance(settings_widget2, SettingsWidget):
            if isinstance(settings_widget1, SettingsWidget):
                if (
                    settings_widget1.label.get_text()
                    < settings_widget2.label.get_text()
                ):
                    return -1
                elif (
                    settings_widget1.label.get_text()
                    == settings_widget2.label.get_text()
                ):
                    return 0
                return 1
            return 1
        return -1

    def settings_filter_func(self, settings_widget):
        query = self.search_entry.get_text().lower()
        if query:
            if isinstance(settings_widget, SettingsWidget):
                result_text = settings_widget.label.get_text().lower()

                ratio = 1
                for word in query.split():
                    ratio *= fuzz.partial_ratio(result_text, word) / 100.0
                if ratio >= 0.8:
                    return True
                return False
            return False
        return True

    def on_key_press(self, widget, event):
        if event.string.strip() and event.string in string.printable:
            if not self.search_entry_revealer.get_reveal_child():
                self.search_entry_revealer.set_reveal_child(True)
                self.search_entry.grab_focus()
                self.search_entry.set_text(event.string)
                self.search_entry.do_move_cursor(
                    self.search_entry, Gtk.MovementStep.BUFFER_ENDS, 1, False
                )
            return False
        elif event.keyval == 65307:  # ESC
            self.search_entry_revealer.set_reveal_child(False)
            self.search_entry.set_text("")

    def on_search_changed(self, editable):
        self.box.invalidate_filter()


class SettingsWidget(Gtk.FlowBoxChild):
    """A widget that is used to represent a single setting. NO BINDINGS."""

    def __init__(self, settings, key, size_group, index=None):
        """Create a new SettingsWidget with label as description and widget as action widget."""
        super().__init__()

        self.settings = settings

        self.key = key

        self.size_group = size_group

        self.index = index
        self.has_children = False

        self.create_action_widget()
        self.connect_settings_listener()
        self.layout()

    def layout(self):
        """Add all necessary sub-widgets to the widget."""
        self.box = Gtk.Box(
            orientation=Gtk.Orientation.HORIZONTAL,
            spacing=self.settings.get("application_padding", 5) * 4,
        )
        self.box.set_border_width(self.settings.get("application_padding", 5))

        self.action_area = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)

        if self.index is None:
            if len(descriptions.get(self.key, "")):
                self.label = Gtk.Label(
                    label="<span weight='medium'>"
                    + key_to_human(self.key)
                    + "</span>"
                    + "\n<span weight='light' size='smaller' style='oblique'>"
                    + descriptions[self.key]
                    + "</span>"
                )
            else:
                self.label = Gtk.Label(
                    label="<span weight='medium'>" + key_to_human(self.key) + "</span>"
                )
        else:
            if (self.key + "#" + str(self.index)) in descriptions:
                self.label = Gtk.Label(
                    label="<span weight='medium'>"
                    + descriptions[self.key + "#" + str(self.index)]
                    + "</span>"
                )
            else:
                self.label = Gtk.Label(
                    label=("<span weight='medium'>" + "#" + str(self.index)) + "</span>"
                )
        self.label.set_use_markup(True)
        self.label.set_line_wrap(True)

        self.label.set_xalign(0)

        self.label.set_halign(Gtk.Align.START)
        self.label.set_valign(Gtk.Align.START)

        if self.index is None:
            self.label.set_margin_top(self.settings.get("application_padding", 5))

        self.box.pack_start(self.label, True, True, 0)

        self.action_area.pack_end(self.action_widget, False, False, 0)

        self.size_group.add_widget(self.action_area)

        self.action_area.set_valign(Gtk.Align.START)
        self.action_area.set_halign(Gtk.Align.START)

        self.box.pack_end(self.action_area, False, False, 0)

        self.add(self.box)

    def create_action_widget(self):
        """
        Create and set SettingsWidget.action_widget for the current type.

        Will take into account metadata (stored in PersistentDict).

        Metadata may look like one of the following:
            Ranges
                Range (int, step=1):        [start (int), end (int)]
                Range (int):                [start (int), end (int), step (int)]
                Range (float, step=1):      [start (int / float), end (int / float)]
                Range (float):              [start (int / float), end (int / float), step (int / float)]
            Choices
                Choice                      [option1, option2, ...]
            Type override
                Type override               typename
                                                May be one of str, int, float, list, bool
        """
        if self.index is None:
            value = self.settings.get(self.key)
            metadata = self.settings.get_metadata(self.key)
        else:
            value = self.settings.get(self.key)[self.index]
            metadata = self.settings.get_metadata(self.key + "#" + str(self.index))

        setting_type = type(value)

        self.setting_change_listener = None

        possible_flags = ""

        if metadata is not None:
            if isinstance(metadata, (tuple, list)):
                if (
                    len(metadata) == 2
                    and isinstance(metadata[0], (float, int))
                    and isinstance(metadata[1], (float, int))
                ):
                    self.action_widget = Gtk.SpinButton.new_with_range(
                        metadata[0], metadata[1], 1
                    )

                    self.action_widget.set_value(value)

                    self.action_widget.connect("value-changed", self.on_number_changed)
                    self.setting_change_listener = self.on_number_setting_changed
                    return
                elif (
                    len(metadata) == 3
                    and isinstance(metadata[0], (float, int))
                    and isinstance(metadata[1], (float, int))
                    and isinstance(metadata[2], (float, int))
                ):
                    self.action_widget = Gtk.SpinButton.new_with_range(
                        metadata[0], metadata[1], metadata[2]
                    )

                    self.action_widget.set_value(value)

                    self.action_widget.connect("value-changed", self.on_number_changed)
                    self.setting_change_listener = self.on_number_setting_changed
                    return
                else:
                    self.action_widget = Gtk.ComboBoxText()

                    for choice in metadata:
                        self.action_widget.append(choice, choice)

                    self.action_widget.set_active_id(value)

                    self.action_widget.connect("changed", self.on_choice_changed)
                    self.setting_change_listener = self.on_choice_setting_changed
                    return
            for _type in TYPES:
                if metadata.startswith(_type):
                    setting_type = TYPES[_type]
                    possible_flags = metadata[len(_type) :]
                    break

        # flags may be True, unset (>count that as False) or a string, in which case
        #           you have to call self.settings.get(string) to get the flag value
        self.flags = {}
        self.flag_listeners = []

        if possible_flags.startswith("(") and metadata.endswith(")"):
            flags = possible_flags[1:-1]
            for flag_str in flags.split("|"):
                flag_str = flag_str.strip()
                if "=" in flag_str:
                    flag_key = flag_str.split("=", 1)[1].strip()
                    self.flags[flag_str.split("=", 1)[0].strip()] = flag_key

                    flag_listener = lambda setting: self.flags.update(
                        {flag_str: setting}
                    )
                    self.flag_listeners.append(flag_listener)
                    self.settings.add_change_listener(flag_key, flag_listener)
                else:
                    self.flags[flag_str] = True

        if setting_type == int:
            self.action_widget = Gtk.SpinButton.new_with_range(0, 50000000, 1)

            self.action_widget.set_value(value)

            self.action_widget.connect("value-changed", self.on_number_changed)
            self.setting_change_listener = self.on_number_setting_changed
        elif setting_type == float:
            self.action_widget = Gtk.SpinButton.new_with_range(0.05, 0.95, 0.05)

            self.action_widget.set_value(value)

            self.action_widget.connect("value-changed", self.on_number_changed)
            self.setting_change_listener = self.on_number_setting_changed
        elif setting_type == bool:
            self.action_widget = Gtk.Switch()
            self.action_widget.set_valign(Gtk.Align.CENTER)

            self.action_widget.set_state(value)

            self.action_widget.connect("state-set", self.on_bool_changed)
            self.setting_change_listener = self.on_bool_setting_changed
        elif (
            setting_type == str
            and (str(value).startswith("#") and len(value) in {7, 4})
            and not metadata == "str"
        ):
            color = value
            rgba = Gdk.RGBA(*colors.rgb255_to_rgb1(colors.hex_to_rgb(color)))

            self.action_widget = Gtk.ColorButton.new_with_rgba(rgba)

            Gtk.ColorChooser.set_use_alpha(self.action_widget, False)

            self.action_widget.connect("color-set", self.on_color_changed)
            self.setting_change_listener = self.on_color_setting_changed
        elif setting_type == str:
            string = value
            if os.path.isdir(os.path.expanduser(string)) and not metadata == "str":
                self.action_widget = Gtk.FileChooserButton(
                    title="preferences_choose_folder_dialog_title",
                    action=Gtk.FileChooserAction.SELECT_FOLDER,
                )
                self.action_widget.set_width_chars(8)

                self.action_widget.set_filename(
                    os.path.abspath(os.path.expanduser(string))
                )

                self.action_widget.connect("file-set", self.on_file_changed)
                self.setting_change_listener = self.on_file_setting_changed
            elif os.path.isfile(os.path.expanduser(string)) and not metadata == "str":
                self.action_widget = Gtk.FileChooserButton(
                    title="preferences_choose_file_dialog_title",
                    action=Gtk.FileChooserAction.OPEN,
                )
                self.action_widget.set_width_chars(8)

                self.action_widget.set_filename(
                    os.path.abspath(os.path.expanduser(string))
                )

                self.action_widget.connect("file-set", self.on_file_changed)
                self.setting_change_listener = self.on_file_setting_changed
            else:
                self.action_widget = Gtk.Entry()
                self.action_widget.set_width_chars(16)

                self.action_widget.set_text(value)

                self.action_widget.connect("changed", self.on_str_changed)
                self.setting_change_listener = self.on_str_setting_changed
        elif setting_type == "FONT":
            self.action_widget = Gtk.FontButton()
            self.action_widget.set_use_font(True)

            self.action_widget.set_font(value)

            self.action_widget.connect("font-set", self.on_font_changed)
            self.setting_change_listener = self.on_font_setting_changed

            if "monospace" in self.flags:
                self.flag_listeners.append(self.on_monospace_flag_changed)
                self.settings.add_change_listener(
                    self.flags["monospace"], self.on_monospace_flag_changed
                )
                self.on_monospace_flag_changed(
                    self.settings.get(self.flags["monospace"])
                )
        elif setting_type == list:
            self.has_children = True

            size_group = Gtk.SizeGroup(mode=Gtk.SizeGroupMode.HORIZONTAL)

            self.action_widget = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
            for index, i in enumerate(value):

                self.action_widget.pack_start(
                    SettingsWidget(self.settings, self.key, size_group, index),
                    True,
                    True,
                    0,
                )
        else:
            self.action_widget = Gtk.Button(label="Unknown settings type")

        if "depends" in self.flags:
            self.flag_listeners.append(self.on_depends_flag_changed)
            self.settings.add_change_listener(
                self.flags["depends"], self.on_depends_flag_changed
            )
            self.on_depends_flag_changed(self.settings.get(self.flags["depends"]))

        elif "!depends" in self.flags:
            self.flag_listeners.append(self.on_not_depends_flag_changed)
            self.settings.add_change_listener(
                self.flags["!depends"], self.on_not_depends_flag_changed
            )
            self.on_not_depends_flag_changed(self.settings.get(self.flags["!depends"]))

    # Flag change listeners

    def on_depends_flag_changed(self, depends):
        self.set_sensitive(depends)
        if depends:
            self.set_opacity(1)
        else:
            self.set_opacity(0.5)

    def on_not_depends_flag_changed(self, depends):
        self.set_sensitive(not depends)
        if not depends:
            self.set_opacity(1)
        else:
            self.set_opacity(0.5)

    def on_monospace_flag_changed(self, monospace):
        if monospace:
            self.action_widget.set_filter_func(
                lambda family, face, *data: family.is_monospace()
            )
        else:
            self.action_widget.set_filter_func(None)

    def connect_settings_listener(self):
        """Connect a listener to the PersistentDict that updates the self.action_widget. create_action_widget MUST have been called."""
        self.setting_changed = False

        if self.setting_change_listener is not None:
            self.settings.add_change_listener(self.key, self.setting_change_listener)

        self.connect("destroy", self.on_destroy)

    # PersistentDict updates
    def on_number_setting_changed(self, setting):
        """Is called when the setting changes in the PersistentDict (Type: int)."""
        self.setting_changed = True
        if self.index is None:
            self.action_widget.set_value(setting)
        else:
            self.action_widget.set_value(setting[self.index])
        self.setting_changed = False

    def on_bool_setting_changed(self, setting):
        """Is called when the setting changes in the PersistentDict (Type: bool)."""
        self.setting_changed = True
        if self.index is None:
            self.action_widget.set_state(setting)
        else:
            self.action_widget.set_state(setting[self.index])
        self.setting_changed = False

    def on_color_setting_changed(self, setting):
        """Is called when the setting changes in the PersistentDict (Type: color)."""
        self.setting_changed = True
        color = Gdk.RGBA()
        if self.index is None:
            color.parse(setting)
        else:
            color.parse(setting[self.index])
        self.action_widget.set_rgba(color)
        self.setting_changed = False

    def on_str_setting_changed(self, setting):
        """Is called when the setting changes in the PersistentDict (Type: str)."""
        self.setting_changed = True
        if self.index is None:
            self.action_widget.set_text(setting)
        else:
            self.action_widget.set_text(setting[self.index])
        self.setting_changed = False

    def on_file_setting_changed(self, setting):
        """Is called when the setting changes in the PersistentDict (Type: file)."""
        self.setting_changed = True
        if self.index is None:
            self.action_widget.set_filename(setting)
        else:
            self.action_widget.set_filename(setting[self.index])
        self.setting_changed = False

    def on_choice_setting_changed(self, setting):
        """Is called when the setting changes in the PersistentDict (Type: choice)."""
        self.setting_changed = True
        if self.index is None:
            self.action_widget.set_active_id(setting)
        else:
            self.action_widget.set_active_id(setting[self.index])
        self.setting_changed = False

    def on_font_setting_changed(self, setting):
        """Is called when the setting changes in the PersistentDict (Type: int)."""
        self.setting_changed = True
        if self.index is None:
            self.action_widget.set_font(setting)
        else:
            self.action_widget.set_font(setting[self.index])
        self.setting_changed = False

    # widget updates
    def on_number_changed(self, editable):
        """Is called when SettingsWidget.action_widget's value is changed (Type: int)."""
        if not self.setting_changed:
            if self.index is None:
                self.settings.set(self.key, self.action_widget.get_value())
            else:
                iterable = self.settings.get(self.key)
                iterable[self.index] = self.action_widget.get_value()
                self.settings.set(self.key, iterable)

    def on_bool_changed(self, switch, state):
        """Is called when SettingsWidget.action_widget's value is changed (Type: bool)."""
        if not self.setting_changed:
            if self.index is None:
                self.settings.set(self.key, state)
            else:
                iterable = self.settings.get(self.key)
                iterable[self.index] = state
                self.settings.set(self.key, iterable)

    def on_color_changed(self, color_button):
        """Is called when SettingsWidget.action_widget's value is changed (Type: color)."""
        if not self.setting_changed:
            if self.index is None:
                self.settings.set(
                    self.key,
                    colors.rgb_to_hex(
                        colors.rgb1a_to_rgb255(tuple(self.action_widget.get_rgba()))
                    ),
                )
            else:
                iterable = self.settings.get(self.key)
                iterable[self.index] = colors.rgb_to_hex(
                    colors.rgb1a_to_rgb255(tuple(self.action_widget.get_rgba()))
                )
                self.settings.set(self.key, iterable)

    def on_str_changed(self, editable):
        """Is called when SettingsWidget.action_widget's value is changed (Type: str)."""
        if not self.setting_changed:
            if self.index is None:
                self.settings.set(self.key, self.action_widget.get_text())
            else:
                iterable = self.settings.get(self.key)
                iterable[self.index] = self.action_widget.get_text()
                self.settings.set(self.key, iterable)

    def on_file_changed(self, editable):
        """Is called when SettingsWidget.action_widget's value is changed (Type: file)."""
        if not self.setting_changed:
            if self.index is None:
                self.settings.set(self.key, self.action_widget.get_filename())
            else:
                iterable = self.settings.get(self.key)
                iterable[self.index] = self.action_widget.get_filename()
                self.settings.set(self.key, iterable)

    def on_choice_changed(self, combo_box):
        """Is called when SettingsWidget.action_widget's value is changed (Type: choice)."""
        if not self.setting_changed:
            if self.index is None:
                self.settings.set(self.key, self.action_widget.get_active_text())
            else:
                iterable = self.settings.get(self.key)
                iterable[self.index] = self.action_widget.get_active_text()
                self.settings.set(self.key, iterable)

    def on_font_changed(self, font_chooser):
        """Is called when SettingsWidget.action_widget's value is changed (Type: choice)."""
        if not self.setting_changed:
            if self.index is None:
                self.settings.set(self.key, self.action_widget.get_font())
            else:
                iterable = self.settings.get(self.key)
                iterable[self.index] = self.action_widget.get_font()
                self.settings.set(self.key, iterable)

    # General GTK signals
    def on_destroy(self, widget):
        """Is called when self is destroyed (e.g. because the window is closing)."""
        if self.setting_change_listener is not None:
            self.settings.remove_change_listener(self.setting_change_listener)

        for listener in self.flag_listeners:
            self.settings.remove_change_listener(listener)


if __name__ == "__main__":
    import persistentdict

    settings = persistentdict.PersistentDict("settings.hjson")

    sew = SettingsEditorWindow(settings)

    sew.connect("destroy", Gtk.main_quit)

    Gtk.main()
