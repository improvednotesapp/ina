"""Handy functions for colours."""

if __package__ is not None and __package__ != "":
    from . import colornames
else:
    import colornames

colornames = colornames.colors


def luminance(color: "(r, g, b)"):
    """Calculate the luminance of a colour."""
    r, g, b = color
    return 0.2126 * r + 0.7152 * g + 0.0722 * b


def contrast(base_color: "(r, g, b)", option1: "(r, g, b)", option2: "(r, g, b)"):
    """Choose the colorr that has the most difference to the base colour in comparison to the base colour and return it."""
    base_lum = luminance(base_color)
    lum1 = luminance(option1)
    lum2 = luminance(option2)

    diff1 = max(base_lum, lum1) - min(base_lum, lum1)
    diff2 = max(base_lum, lum2) - min(base_lum, lum2)

    if diff1 > diff2:
        return option1
    return option2


def rgb_to_hex(rgb_color):
    return "#%02x%02x%02x" % rgb_color


def string_to_rgb255(string):
    """
    Return a tuple of length 3 or 4 (if including alpha) that equals to the color given in the string.

    The string may be in one of the following formats:
        "red" -> (255, 0, 0)
        "#f00" -> (255, 0, 0)
        "#f00f" -> (255, 0, 0, 255)
        "#ff0000" -> (255, 0, 0)
        "#ff0000ff" -> (255, 0, 0, 255)
        "rgb(255, 0, 0)" -> (255, 0, 0)
        "rgba(255, 0, 0, 1)" -> (255, 0, 0, 255)
        "rgb(1, 0, 0)" -> (255, 0, 0)
        "rgb(1, 0, 0, 1)" -> (255, 0, 0, 255)
    """
    if string.startswith("#"):
        return hex_to_rgb(string)

    elif string.startswith("rgb(") and string.endswith(")"):
        vals = string[4:-1].split(",")

        if len(vals) == 3:
            if "." in vals[0] or all([int(val) <= 1 for val in vals]):
                return tuple([int(float(val) * 255) for val in vals])

            return tuple([int(val) for val in vals])

    elif string.startswith("rgba(") and string.endswith(")"):
        vals = string[5:-1].split(",")

        if len(vals) == 4:
            if "." in vals[0] or all([float(val) <= 1 for val in vals]):
                return tuple([int(float(val) * 255) for val in vals])

            return tuple([int(val) for val in vals[:-1]] + [int(float(vals[-1]) * 255)])

    elif string in colornames:
        return string_to_rgb255(colornames[string])
    return None


def hex_to_rgb(hex_color):
    hex_color = hex_color.lstrip("#")
    if len(hex_color) == 8:
        return tuple(
            int(hex_color[i : i + len(hex_color) // 4], 16)
            for i in range(0, len(hex_color), len(hex_color) // 4)
        )
    elif len(hex_color) == 6:
        return tuple(
            int(hex_color[i : i + len(hex_color) // 3], 16)
            for i in range(0, len(hex_color), len(hex_color) // 3)
        )
    elif len(hex_color) == 4:
        return hex_to_rgb(
            "#"
            + (hex_color[0] * 2)
            + (hex_color[1] * 2)
            + (hex_color[2] * 2)
            + (hex_color[3] * 2)
        )
    elif len(hex_color) == 3:
        return hex_to_rgb(
            "#" + (hex_color[0] * 2) + (hex_color[1] * 2) + (hex_color[2] * 2)
        )


def rgb255_to_rgb1(rgb_color):
    return tuple([c / 255 for c in tuple(rgb_color)])


def rgb1a_to_rgb255(rgb_color):
    r, g, b, *a = rgb_color
    return tuple([int(c * 255) for c in (r, g, b)])


if __name__ == "__main__":
    [
        print(rgb255_to_rgb1(string_to_rgb255(color)))
        for color in (
            "#00112233",
            "#001122",
            "#012",
            "#0123",
            "red",
            "green",
            "rgb(5, 99, 128)",
            "rgb(.5, .6, .4)",
            "rgba(.5, .6, .4, .4)",
            "rgba(1, .6, .4, .4)",
            "rgba(124, 65, 83, .8)",
        )
    ]
