# main.py
#
# Copyright 2020 Czarlie
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE X CONSORTIUM BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
# Except as contained in this notice, the name(s) of the above copyright
# holders shall not be used in advertising or otherwise to promote the sale,
# use or other dealings in this Software without prior written
# authorization.

from typing import Dict, Type, Any, Optional

import sys
from os import listdir, mkdir, path, sep

from importlib import import_module
from inspect import isabstract
import hashlib

from gi.repository import Gdk, Gtk, Gio, Notify

from .window import InaWindow
from .persistentdict import PersistentDict
from .segments.__segment__ import Segment


Notify.init("org.czarlie.Ina")


SEGMENT_TYPES: Dict[str, Type[Segment]] = {}


def load_segments(directory: str):
    for segment_filename in listdir(directory):
        segment_path = path.join(directory, segment_filename)

        if not segment_filename.startswith("__"):
            if path.isfile(segment_path) and segment_filename.endswith(".py"):
                import_path = "."

                for path_part in segment_path.split(sep)[1:-1]:
                    import_path += path_part + "."

                # strip ".py"
                import_path += segment_filename[::-1].replace(".py"[::-1], "", 1)[::-1]

                segment_module = import_module(import_path, package=__package__,)

                for name in dir(segment_module):
                    if not name.startswith("__"):
                        obj: Any = getattr(segment_module, name)
                        if (
                            isinstance(obj, type)
                            and issubclass(obj, Segment)
                            and obj not in SEGMENT_TYPES
                            and not isabstract(obj)
                        ):
                            sha256 = hashlib.sha256()

                            sha256.update(segment_path.encode())
                            sha256.update(b"//")
                            sha256.update(name.encode())

                            SEGMENT_TYPES[sha256.hexdigest()] = obj

            elif segment_path and path.isfile(path.join(segment_path, "__init__.py")):
                load_segments(segment_path)


load_segments("src/segments")

Gtk.Settings.get_default().props.gtk_shell_shows_app_menu = False


class Application(Gtk.Application):
    def __init__(self):
        super().__init__(application_id="org.czarlie.Ina", flags=Gio.ApplicationFlags.FLAGS_NONE)

        self.add_actions()

        self.is_startup = True

        self.open_notes = {}

        self.pdict = PersistentDict("app.json")

        css_provider = Gtk.CssProvider()
        css_provider.load_from_resource("org/czarlie/Ina/ina.css")
        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(), css_provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION,
        )

        self.last_notification: Optional[Notify.Notification] = None
        # self.set_inactivity_timeout(10000)

    def add_actions(self):
        quit_action = Gio.SimpleAction.new("quit", None)
        quit_action.connect("activate", self.on_quit)
        self.add_action(quit_action)

    def do_activate(self):
        if self.is_startup:
            note_count = 0

            try:
                mkdir("notes")
            except FileExistsError:
                pass

            for note in listdir("notes"):
                if not PersistentDict(path.join("notes", note)).get("archived", False):
                    note_id = note.split(".json", 1)[0]
                    note_count += 1
                    self.show_note(note_id)

            if note_count == 0:
                if (not self.is_startup) or "--no-new-note" not in sys.argv:
                    self.on_new_note()

            if "update_note.json" in listdir():
                update_note_win = InaWindow(SEGMENT_TYPES, "update_note.json", application=self)
                update_note_win.present()

            self.is_startup = False

        elif "--no-new-note" not in sys.argv:
            self.on_new_note()

    def on_new_note(self, *args):
        self.show_note()

    def show_note(self, note_id: Optional[str] = None) -> Optional[InaWindow]:
        note_win: Optional[InaWindow] = None

        if note_id not in self.open_notes:
            note_win = InaWindow(SEGMENT_TYPES, note_id, application=self)
            self.open_notes[note_win.note_id] = note_win
            self.add_window(note_win)

        # self.load_plugins(note_win)

        return note_win

    def on_quit(self, *args):
        for win in self.get_windows():
            win.close()


def main():
    app = Application()
    return app.run()
